﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.Web.Api.Resource.V2.Models
{
    /// <summary>
    /// The product object
    /// </summary>
    public class ProductEditModel
    {
        /// <summary>
        /// Unique id of the product
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// Name of the product
        /// </summary>
        [Required]
        public String Name { get; set; }

        /// <summary>
        /// Produc price
        /// </summary>
        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }

        /// <summary>
        /// Description of the product
        /// </summary>
        [MaxLength(500)]
        public String Description { get; set; }

        /// <summary>
        /// Record version
        /// </summary>
        [Required]
        public String Version { get; set; }
    }
}
