﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Suitsupply.Catalog.Data.Entities;
using Suitsupply.Catalog.Data.UnitsOfWork;
using Suitsupply.Catalog.Web.Api.Resource.V2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Suitsupply.Catalog.Serialization;
using Suitsupply.Catalog.Web.Api.Resource.ErrorHandling;

namespace Suitsupply.Catalog.Web.Api.Resource.V2.Controllers
{
    /// <summary>
    /// Sample Catalog API
    /// </summary>
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductsController : BaseController
    {
        /// <summary>
        /// Controller constructor inherited from abstract class BaseController
        /// </summary>
        /// <param name="unitOfWork">EF Core UnitOfWork</param>
        /// <param name="mapper">AutoMapper instance</param>
        /// <param name="configuration">Application consfiguration</param>
        /// <param name="logger">Application logger instance</param>
        /// /// <param name="distributedCache">Cache implementation</param>
        public ProductsController(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, ILogger<BaseController> logger, IDistributedCache distributedCache) : base(unitOfWork, mapper, configuration, logger, distributedCache)
        {
        }

        /// <summary>
        /// Get list of products
        /// </summary>
        /// <returns>List of products</returns>
        [HttpGet]
        public async Task<IEnumerable<ProductViewModel>> Get()
        {
            return await this.GetCachedValueAsync(
                "ProductsV1",
                () => this.mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(this.UnitOfWork.Products.Get())
                );
        }

        /// <summary>
        /// Gets the single Product based on the ID
        /// </summary>
        /// <param name="id">Unique ID of the product</param>
        /// <returns>Single product</returns>
        [HttpGet("{id}",Name = "Product")]
        [ProducesResponseType(typeof(ProductEditModel), 200)]
        [ProducesResponseType(204)]
        public async Task<ActionResult> Get(Guid id)
        {
            var product = await this.UnitOfWork.Products.GetByIdAsync(id);
            if (product != null)
            {
                return Ok(this.mapper.Map<Product, ProductEditModel>(product));
            }
            return NoContent();
        }

        /// <summary>
        /// Adds the new product 
        /// </summary>
        /// <param name="product">Product intance</param>
        /// <returns>Path to a product resource</returns>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), 201)]
        public async Task<IActionResult> Post([FromBody] ProductAddModel product)
        {
            var entity = this.mapper.Map<ProductAddModel, Product>(product);
            await this.UnitOfWork.Products.InsertAsync(entity);
            await UnitOfWork.SaveAsync();
            await this.InvalidateCache("ProductsV2");
            return CreatedAtRoute(routeName: "Product", routeValues: new { id = entity.Id.ToString() }, value: entity.Id);
        }

        /// <summary>
        /// Updates the product
        /// </summary>
        /// <param name="id">Product id</param>
        /// <param name="product">Product data</param>
        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(409)]
        public async Task<IActionResult> Patch(Guid id, [FromBody] ProductEditModel product)
        {
            this.UnitOfWork.Update<Product>(this.mapper.Map<ProductEditModel, Product>(product));
            try
            {
                await UnitOfWork.SaveAsync();
                return Ok();
            }
            catch(DbUpdateConcurrencyException ex)
            {
                this.logger.LogError(ex, "Databse concurrency conflict");
                return Conflict();
            }
        }

        /// <summary>
        /// Delets the product
        /// </summary>
        /// <param name="id">Product id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            this.UnitOfWork.Products.Remove(id);
            await UnitOfWork.SaveAsync();
        }
    }
}
