﻿using Newtonsoft.Json;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.Web.Api.Resource.V1.Models
{
    /// <summary>
    /// The product object
    /// </summary>
    [Serializable]
    [ProtoContract]
    public class ProductViewModel
    {
        /// <summary>
        /// Unique id of the product
        /// </summary>
        [Required]
        [ProtoMember(1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Name of the product
        /// </summary>
        [Required]
        [ProtoMember(2)]
        public String Name { get; set; }

        /// <summary>
        /// Produc price
        /// </summary>
        [Required]
        [Range(0, double.MaxValue)]
        [ProtoMember(3)]
        public double Price { get; set; }

        /// <summary>
        /// Date and time product information was last updated
        /// </summary>
        [ProtoMember(4)]
        public DateTime LastUpdated { get; set; }


    }
}
