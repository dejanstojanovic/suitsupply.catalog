﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.Web.Api.Resource.V1.Models
{
    /// <summary>
    /// The product object
    /// </summary>
    public class ProductAddModel
    {
        /// <summary>
        /// Name of the product
        /// </summary>
        [Required]
        public String Name { get; set; }

        /// <summary>
        /// Produc price
        /// </summary>
        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }



    }
}
