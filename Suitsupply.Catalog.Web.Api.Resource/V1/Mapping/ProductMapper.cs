﻿using AutoMapper;
using Suitsupply.Catalog.Data.Entities;
using System;
using System.Text;

namespace Suitsupply.Catalog.Web.Api.Resource.V1.Mapping
{
    /// <summary>
    /// Mapping profile for Models and Entities
    /// </summary>
    public class ProductMappingProfile : Profile
    {
        /// <summary>
        /// Mapper constructor
        /// </summary>
        public ProductMappingProfile()
        {

            CreateMap<Product, V1.Models.ProductViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.LastUpdated, opt => opt.MapFrom(src => src.LastUpdated));


            CreateMap<V1.Models.ProductAddModel, Product>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.LastUpdated, opt => opt.MapFrom(src => DateTime.Now.ToUniversalTime()));


            CreateMap<V1.Models.ProductEditModel, Product>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.LastUpdated, opt => opt.MapFrom(src => DateTime.Now.ToUniversalTime()))
                .ForMember(dest => dest.Version, opt => opt.MapFrom(src => Convert.FromBase64String(src.Version)));

            CreateMap<Product, V1.Models.ProductEditModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.Version, opt => opt.MapFrom(src => Convert.ToBase64String(src.Version)));

        }

    }
}

