﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Suitsupply.Catalog.Web.Api.Resource.ErrorHandling
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        private ILogger<ApiExceptionFilter> logger;

        public ApiExceptionFilter(ILogger<ApiExceptionFilter> logger)
        {
            this.logger = logger;
        }
        public override void OnException(ExceptionContext context)
        {
            var errorMessage = new ExceptionMessage(context);

            if (context.ModelState.ErrorCount == 0)
            {
                //Unhandled exception
                context.Result = new ErrorObjectResult(errorMessage);
                logger.LogError(context.Exception, context.Exception.Message);
            }
            else
            {
                //Validation exception
                context.Result = new ErrorObjectResult(errorMessage,HttpStatusCode.BadRequest);
            }
            
            base.OnException(context);
        }
    }
}
