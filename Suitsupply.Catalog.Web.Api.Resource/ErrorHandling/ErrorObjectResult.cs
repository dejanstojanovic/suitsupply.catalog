﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Suitsupply.Catalog.Web.Api.Resource.ErrorHandling
{
    public class ErrorObjectResult: ObjectResult
    {
        public ErrorObjectResult(object value, HttpStatusCode statusCode = HttpStatusCode.InternalServerError) : base(value)
        {
            StatusCode = (int)statusCode;
        }
    }
}
