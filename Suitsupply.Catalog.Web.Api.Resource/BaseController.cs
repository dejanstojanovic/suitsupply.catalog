﻿using AutoMapper;
using Suitsupply.Catalog.Data.UnitsOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Distributed;
using System.Threading.Tasks;
using System;
using Suitsupply.Catalog.Serialization;

namespace Suitsupply.Catalog.Web.Api.Resource
{
    public abstract class BaseController : Controller
    {
        #region Fields
        protected readonly IDistributedCache distributedCache;
        protected readonly IConfiguration configuration;
        protected readonly ILogger logger;
        protected readonly IUnitOfWork untiOfWork;
        protected readonly IMapper mapper;
        #endregion

        #region Constructors

        /// <summary>
        /// Controller base constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        /// <param name="distributedCache"></param>
        protected BaseController(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration,
            ILogger<BaseController> logger,
            IDistributedCache distributedCache
            )
        {
            this.untiOfWork = unitOfWork;
            this.configuration = configuration;
            this.logger = logger;
            this.mapper = mapper;
            this.distributedCache = distributedCache;
        }
        #endregion

        /// <summary>
        /// Removes the value from the distributed cache
        /// </summary>
        /// <param name="keys">Keys to be removed from the cache</param>
        /// <returns></returns>
        protected async Task InvalidateCache(params string[] keys)
        {
            foreach(var key in keys)
            {
                await this.distributedCache.RemoveAsync(key);
            }

        }

        /// <summary>
        /// Gets value from the cache or executes function and sets the cache upon execution
        /// </summary>
        /// <typeparam name="T">Type to return</typeparam>
        /// <param name="key">Key for distributed cache</param>
        /// <param name="func">Methods that return value of T</param>
        /// <returns>Instance of generic type T</returns>
        protected async Task<T> GetCachedValueAsync<T>(String key,Func<T> func) where T:class
        {
            var result = await distributedCache.GetValueAsync<T>(key);
            if (result == null)
            {
                result = func();
                await distributedCache.SetValueAsync<T>(key, result, TimeSpan.FromSeconds(this.Configuration.GetValue<int>("CacheExpiry")));
            }
            return result;
        }


        #region Properties
        public IConfiguration Configuration => this.configuration;
        public ILogger Logger => this.logger;
        public IUnitOfWork UnitOfWork => this.untiOfWork;
        #endregion


    }
}
