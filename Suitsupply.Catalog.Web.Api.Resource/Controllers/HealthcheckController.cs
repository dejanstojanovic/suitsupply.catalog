﻿using System.Threading.Tasks;
using AutoMapper;
using Suitsupply.Catalog.Data.UnitsOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Distributed;

namespace Suitsupply.Catalog.Web.Api.Resource.Controllers
{
    /// <summary>
    /// Health check contoller
    /// </summary>
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class HealthcheckController : BaseController
    {

        /// <summary>
        /// Controller constructor inherited from abstract class BaseController
        /// </summary>
        /// <param name="unitOfWork">EF Core UnitOfWork</param>
        /// <param name="mapper">AutoMapper instance</param>
        /// <param name="configuration">Application consfiguration</param>
        /// <param name="logger">Application logger instance</param>
        /// <param name="distributedCache">Cache implementation</param>
        public HealthcheckController(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration, ILogger<BaseController> logger, IDistributedCache distributedCache) : base(unitOfWork, mapper, configuration, logger, distributedCache)
        {
        }

        /// <summary>
        /// Returns the health status of the service instance
        /// </summary>
        /// <returns>HTTP 200 OK for health instance, anything else is considered unhealthy</returns>
        [HttpGet]

        public async Task Get()
        {
            //TODO: Add healtch checks for the instance
            await Task.CompletedTask;
        }
    }
}