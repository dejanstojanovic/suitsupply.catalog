﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Suitsupply.Catalog.Web.App.Models
{
    public class ProductEditModel
    {
        public Guid Id { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }

        [MaxLength(500)]
        public String Description { get; set; }

        //[Required]
        public IFormFile Photo { get; set; }

        public String Version { get; set; }
    }
}
