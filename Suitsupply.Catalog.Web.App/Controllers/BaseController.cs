﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Suitsupply.Catalog.Web.App.Controllers
{
   public abstract class BaseController : Controller
    {
        protected readonly IConfiguration configuration;
        public BaseController(IConfiguration configuration) {
            this.configuration = configuration;
        }
    }
}