﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Suitsupply.Catalog.Web.App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Suitsupply.Catalog.Web.App.Extensions;
using Newtonsoft.Json;
using System.IO;

namespace Suitsupply.Catalog.Web.App.Controllers
{
    [Route("Catalog")]
    public class CatalogController : BaseController
    {
        private readonly String resourceApiUrl;
        private readonly String imagesApiUrl;

        public CatalogController(IConfiguration configuration) : base(configuration)
        {
            this.resourceApiUrl = this.configuration.GetValue<String>("ResourceApiUrl");
            this.imagesApiUrl = this.configuration.GetValue<String>("ImageApiUrl");
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(new Uri($"{this.resourceApiUrl}/Products"));
                var data = JsonConvert.DeserializeObject<IEnumerable<ProductViewModel>>(await result.Content.ReadAsStringAsync());
                return View(data);
            }
        }

        [HttpGet]
        [Route("Edit")]
        public async Task<IActionResult> Edit(Guid id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(new Uri($"{this.resourceApiUrl}/Products/{id}"));
                var model = JsonConvert.DeserializeObject<ProductEditModel>(await result.Content.ReadAsStringAsync());
                return View(model);
            }
        }

        [HttpPost]
        [Route("Edit")]
        public async Task<IActionResult> Edit(ProductEditModel model)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    string patchJsonData = JsonConvert.SerializeObject(new
                    {
                        model.Id,
                        model.Name,
                        model.Description,
                        model.Price,
                        model.Version
                    });
                    var result = await client.PatchAsync(
                        $"{this.resourceApiUrl}/Products/{model.Id}",
                        new StringContent(patchJsonData, System.Text.Encoding.UTF8, "application/json")
                        );

                    if (result.StatusCode == System.Net.HttpStatusCode.OK && model.Photo != null && model.Photo.Length > 0)
                    {
                        using (var imageClient = new HttpClient())
                        {
                            var newId = JsonConvert.DeserializeObject<string>(await result.Content.ReadAsStringAsync());
                            var url = $"{this.imagesApiUrl}/Images/{newId}";
                            var uploadResult = await imageClient.PostAsync(
                                new Uri(url),
                                model.Photo.ToHttpFileContent());
                        }

                    }
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("Add")]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddPost(ProductAddModel model)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    var result = await client.PostAsync<ProductAddModel>(
                        new Uri($"{this.resourceApiUrl}/Products"),
                        model,
                        new JsonMediaTypeFormatter());

                    if (result.StatusCode == System.Net.HttpStatusCode.Created && model.Photo != null && model.Photo.Length > 0)
                    {
                        using (var imageClient = new HttpClient())
                        {
                            var newId = JsonConvert.DeserializeObject<string>(await result.Content.ReadAsStringAsync());
                            var url = $"{this.imagesApiUrl}/Images/{newId}";
                            var uploadResult = await imageClient.PostAsync(
                                new Uri(url),
                                model.Photo.ToHttpFileContent());
                        }
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        [Route("Delete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync(new Uri($"{this.resourceApiUrl}/Products/{id}"));
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    using (var imageClient = new HttpClient())
                    {
                        await imageClient.DeleteAsync(new Uri($"{this.imagesApiUrl}/{await result.Content.ReadAsStringAsync()}"));
                    }
                }
                return RedirectToAction("Index");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}