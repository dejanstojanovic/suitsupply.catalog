﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Suitsupply.Catalog.Web.Api.Resource.Images.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthcheckController : ControllerBase
    {

        [HttpGet]
        public IActionResult Get()
        {
            //TODO: Perform storage availability check
            return Ok();
        }
    }
}