﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Suitsupply.Catalog.FileStorage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Suitsupply.Catalog.Web.Api.Resource.Images.Controllers
{
    /// <summary>
    /// Images provider service
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private IFileStorageProvider fileProvider;

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="fileProvider"></param>
        public ImagesController(IFileStorageProvider fileProvider)
        {
            this.fileProvider = fileProvider;
        }

        /// <summary>
        /// Listiong all present files
        /// </summary>
        /// <returns>List of present file IDs </returns>
        public async Task<IEnumerable<Guid>> Get()
        {
            return await this.fileProvider.ListAsync();
        }


        /// <summary>
        /// Return the image content for the id
        /// </summary>
        /// <param name="id">Unique ID asociated with the image</param>
        /// <returns>Image content</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            var file = await this.fileProvider.GetAsync(id);
            if (file != null)
            {
                return File(file, "image/jpeg");
            }
            return NotFound();
        }

        /// <summary>
        /// Uploads the photo for the Product
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <param name="file">Picture file</param>
        /// <returns></returns>
        [HttpPost("{id}")]
        public async Task Post(Guid id, IFormFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                await this.fileProvider.SaveAsync(id, memoryStream.ToArray());
            }
        }

        /// <summary>
        /// Deletes the image associated with the ID
        /// </summary>
        /// <param name="id">Unique identifier associated with the image</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await this.fileProvider.DeleteAsync(id);
        }
    }
}
