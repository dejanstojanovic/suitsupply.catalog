﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Suitsupply.Catalog.FileStorage
{
    /// <summary>
    /// In memory storage provider for unit testing purposes
    /// </summary>
    public class MemoryStorageProvider : IFileStorageProvider
    {
        private readonly Dictionary<Guid, byte[]> storage;

        public MemoryStorageProvider()
        {
            this.storage = new Dictionary<Guid, byte[]>();
        }

        public void Delete(Guid id)
        {
            this.storage.Remove(id);
        }

        public async Task DeleteAsync(Guid id)
        {
            await Task.FromResult(this.storage.Remove(id));
        }

        public byte[] Get(Guid id)
        {
            if (this.storage.TryGetValue(id, out var data))
            {
                return data;
            }
            return null;
        }

        public async Task<byte[]> GetAsync(Guid id)
        {
            return await Task.FromResult(Get(id));
        }

        public IEnumerable<Guid> List()
        {
            return this.storage.Keys;
        }

        public async Task<IEnumerable<Guid>> ListAsync()
        {
            return await Task.FromResult(this.storage.Keys);
        }

        public void Save(Guid id, byte[] data, bool overwrite = true)
        {
            if (this.storage.ContainsKey(id))
            {
                if (overwrite)
                {
                    this.storage[id] = data;
                }
                else
                {
                    throw new ObjectExistsInStorageException("Object already exists");
                }
            }
            else
            {
                this.storage.Add(id, data);
            }
        }

        public async Task SaveAsync(Guid id, byte[] data, bool overwrite = true)
        {
            Save(id, data, overwrite);
            await Task.CompletedTask;
        }
    }
}
