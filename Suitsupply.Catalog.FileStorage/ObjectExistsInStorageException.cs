﻿using System;

namespace Suitsupply.Catalog.FileStorage
{
    /// <summary>
    /// Thrown when trying to overwite existing file in the storage
    /// </summary>
    public class ObjectExistsInStorageException : Exception
    {
        /// <summary>
        /// Exception constructor
        /// </summary>
        /// <param name="message">Message of the exception</param>
        public ObjectExistsInStorageException(string message) : base(message)
        {
        }
    }
}
