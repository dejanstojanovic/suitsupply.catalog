﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace Suitsupply.Catalog.FileStorage
{
    /// <summary>
    /// Simple file storage provider for working with FileSystem
    /// </summary>
    public class PhysicalStorageProvider : IFileStorageProvider
    {
        private readonly String rootPath;

        /// <summary>
        /// Provider constructor
        /// </summary>
        /// <param name="root">Folder path where files will be stored</param>
        /// <param name="createFolderIfMissing">Create storage folder if it does not exist in the storage root folder</param>
        public PhysicalStorageProvider(String root, bool createFolderIfMissing = true)
        {
            this.rootPath = root;
            if(!Directory.Exists(root)&& createFolderIfMissing)
            {
                Directory.CreateDirectory(root);
            }
        }

        /// <summary>
        /// Retrieves list of stored files
        /// </summary>
        /// <returns>List of filenames stored in the storage</returns>
        public IEnumerable<Guid> List()
        {
            return Directory.GetFiles(rootPath).Select(f => new Guid(Path.GetFileName(f)));
        }

        /// <summary>
        /// Retrieves list of stored files asynchnously
        /// </summary>
        /// <returns>List of filenames stored in the storage</returns>
        public async Task<IEnumerable<Guid>> ListAsync()
        {
            return await Task.FromResult(Directory.GetFiles(rootPath).Select(f=> new Guid(Path.GetFileName(f))));
        }

        /// <summary>
        /// Gets the content of the file for specific ID
        /// </summary>
        /// <param name="id">Unique identifier of the file</param>
        /// <returns>File content as byte array</returns>
        public byte[] Get(Guid id)
        {
            //TODO: Consider caching the result to avoid expensive IO operation every time
            return File.ReadAllBytes(Path.Combine(rootPath, id.ToString()));
        }

        /// <summary>
        /// Gets the content of the file for specific ID asynchronously
        /// </summary>
        /// <param name="id">Unique identifier of the file</param>
        /// <returns>File content as byte array</returns>
        public async Task<byte[]> GetAsync(Guid id)
        {
            return await File.ReadAllBytesAsync(Path.Combine(rootPath, id.ToString()));
        }

        /// <summary>
        /// Stores the file content 
        /// </summary>
        /// <param name="id">Unique identifier of the file</param>
        /// <param name="data">File body</param>
        /// <param name="overwrite">Overwrite the file if exists. If this is false and file exists, exception will be thrown</param>
        public void Save(Guid id, byte[] data, bool overwrite = true)
        {
            if(File.Exists(Path.Combine(rootPath, id.ToString())))
            {
                if (overwrite)
                {
                    File.WriteAllBytes(Path.Combine(rootPath, id.ToString()), data);
                }
                else
                {
                    throw new ObjectExistsInStorageException("Object already exists");
                }
            }
            else
            {
                File.WriteAllBytes(Path.Combine(rootPath, id.ToString()), data);
            }
        }

        /// <summary>
        /// Stores the file content asynchronously
        /// </summary>
        /// <param name="id">Unique identifier of the file</param>
        /// <param name="data">File body</param>
        /// <param name="overwrite">Overwrite the file if exists. If this is false and file exists, exception will be thrown</param>
        public async Task SaveAsync(Guid id, byte[] data, bool overwrite = true)
        {
            if (File.Exists(Path.Combine(rootPath, id.ToString())))
            {
                if (overwrite)
                {
                    await File.WriteAllBytesAsync(Path.Combine(rootPath, id.ToString()), data);
                }
                else
                {
                    throw new ObjectExistsInStorageException("Object already exists");
                }
            }
            else
            {
                await File.WriteAllBytesAsync(Path.Combine(rootPath, id.ToString()), data);
            }
        }

        /// <summary>
        /// Delete the file from the storage
        /// </summary>
        /// <param name="id">Unique file identifier</param>
        public void Delete(Guid id)
        {
            File.Delete(Path.Combine(rootPath, id.ToString()));
        }

        /// <summary>
        /// Delete the file from the storage asynchronously
        /// </summary>
        /// <param name="id">Unique file identifier</param>
        public async Task DeleteAsync(Guid id)
        {
            File.Delete(Path.Combine(rootPath, id.ToString()));
            await Task.CompletedTask;
        }

        
 
    }
}
