﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.FileStorage
{
    public interface IFileStorageProvider
    {
        IEnumerable<Guid> List();
        Task<IEnumerable<Guid>> ListAsync();
        byte[] Get(Guid id);
        Task<byte[]> GetAsync(Guid id);
        void Save(Guid id, byte[] data, bool overwrite = true);
        Task SaveAsync(Guid id, byte[] data, bool overwrite = true);
        void Delete(Guid id);
        Task DeleteAsync(Guid id);
    }
}
