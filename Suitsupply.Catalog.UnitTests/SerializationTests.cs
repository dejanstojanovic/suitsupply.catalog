﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProtoBuf;
using Suitsupply.Catalog.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.UnitTests
{
    [ProtoContract]
    class SerializationTestModel
    {
        [ProtoMember(1)]
        public Guid Id { get; set; }
        [ProtoMember(2)]
        public String Name { get; set; }
        [ProtoMember(3)]
        public String Description { get; set; }
    }


    [TestClass]
   public class SerializationTests
    {
        ISerializer protobufSerializer = new ProtobufSerializer();

        [TestCategory("Serialization")]
        [TestMethod]
        public async Task ProtobufSerialize()
        {
            var model = new SerializationTestModel()
            {
                Id = Guid.NewGuid(),
                Name = "TestModel",
                Description = "This is test model"
            };

            var data = protobufSerializer.Serialize<SerializationTestModel>(model);
            Assert.IsTrue(data != null && data.Any());
        }


        [TestCategory("Serialization")]
        [TestMethod]
        public async Task ProtobufDeserialize()
        {
            var model = new SerializationTestModel()
            {
                Id = Guid.NewGuid(),
                Name = "TestModel",
                Description = "This is test model"
            };

            var data = protobufSerializer.Serialize<SerializationTestModel>(model);

            var result = protobufSerializer.Deserialize<SerializationTestModel>(data);

            Assert.IsTrue(result != null && result is SerializationTestModel);
        }


    }
}
