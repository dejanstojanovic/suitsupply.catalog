﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Suitsupply.Catalog.Data.Entities;
using Suitsupply.Catalog.Data.Repositories;
using Suitsupply.Catalog.Data.UnitsOfWork;
using Suitsupply.Catalog.Web.Api.Resource.V1.Controllers;
using Suitsupply.Catalog.Web.Api.Resource.V1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Suitsupply.Catalog.UnitTests.V1
{
    [TestClass]
    public class ProductController_Tests
    {
        Mock<IUnitOfWork> unitOfWork;
        Mock<IProductRepository> repository;
        Mock<IMapper> mapper;
        Mock<IConfiguration> configuration;
        Mock<ILogger<ProductsController>> logger;
        Mock<IDistributedCache> distributedCache;

        [TestInitialize]
        public void Initialize()
        {
            this.unitOfWork = new Mock<IUnitOfWork>();
            this.mapper = new Mock<IMapper>();
            this.configuration = new Mock<IConfiguration>();
            this.logger = new Mock<ILogger<ProductsController>>();
            this.distributedCache = new Mock<IDistributedCache>();

            repository = new Mock<IProductRepository>();
            this.unitOfWork.Setup(u => u.Products).Returns(repository.Object);

            this.mapper.Setup(m => m.Map<Product, ProductViewModel>(It.IsAny<Product>())).Returns(It.IsAny<ProductViewModel>());
            this.mapper.Setup(m => m.Map<Product, ProductEditModel>(It.IsAny<Product>())).Returns(It.IsAny<ProductEditModel>());


            this.distributedCache.Setup(dc => dc.GetAsync(It.IsAny<String>(), default(CancellationToken))).ReturnsAsync(It.IsAny<byte[]>());
            this.configuration.Setup(c => c.GetSection(It.IsAny<String>())).Returns(new Mock<IConfigurationSection>().Object);
        }

        [TestCleanup]
        public void CleanUp()
        {

        }

        [TestMethod]
        public async Task Get_Test_Ok()
        {
            this.unitOfWork.Setup(u => u.Products.Get()).Returns(It.IsAny<IQueryable<Product>>());

            using (var controller = new ProductsController(
                this.unitOfWork.Object,
                this.mapper.Object,
                this.configuration.Object,
                this.logger.Object,
                this.distributedCache.Object))
            {

                var result = await controller.Get();
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            }
        }

        [TestMethod]
        public async Task GetById_Test_Ok()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            this.unitOfWork.Setup(u => u.Products).Returns(repositoryMock.Object);
            this.unitOfWork.Setup(u => u.Products.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(new Product());

            using (var controller = new ProductsController(
                this.unitOfWork.Object,
                this.mapper.Object,
                this.configuration.Object,
                this.logger.Object,
                this.distributedCache.Object))
            {

                var result = await controller.Get(Guid.NewGuid());
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            }

        }


        [TestMethod]
        public async Task GetById_Test_NotFound()
        {
            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            this.unitOfWork.Setup(u => u.Products).Returns(repositoryMock.Object);
            this.unitOfWork.Setup(u => u.Products.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Product)null);

            using (var controller = new ProductsController(
                this.unitOfWork.Object,
                this.mapper.Object,
                this.configuration.Object,
                this.logger.Object,
                this.distributedCache.Object))
            {

                var result = await controller.Get(Guid.NewGuid());
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(NoContentResult));
            }

        }

        [TestMethod]
        public async Task Post_Test_CreatedAt()
        {
            this.mapper.Setup(m => m.Map<ProductAddModel, Product>(It.IsAny<ProductAddModel>())).Returns(new Product()
            {
                Id = Guid.NewGuid()
            });

            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            this.unitOfWork.Setup(u => u.Products).Returns(repositoryMock.Object);
            this.unitOfWork.Setup(u => u.Products.InsertAsync(It.IsAny<Product>())).ReturnsAsync(It.IsAny<Guid>());

            using (var controller = new ProductsController(
                this.unitOfWork.Object,
                this.mapper.Object,
                this.configuration.Object,
                this.logger.Object,
                this.distributedCache.Object))
            {
                var result = await controller.Post(new ProductAddModel()
                {
                    Name = "Test product",
                    Price = 1
                });
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(CreatedAtRouteResult));
            }

        }

        [TestMethod]
        public async Task Patch_Test_Ok()
        {
            this.mapper.Setup(m => m.Map<ProductAddModel, Product>(It.IsAny<ProductAddModel>())).Returns(new Product()
            {
                Id = Guid.NewGuid()
            });

            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            this.unitOfWork.Setup(u => u.Products).Returns(repositoryMock.Object);
            this.unitOfWork.Setup(u => u.Update<Product>(It.IsAny<Product>()));

            using (var controller = new ProductsController(
                this.unitOfWork.Object,
                this.mapper.Object,
                this.configuration.Object,
                this.logger.Object,
                this.distributedCache.Object))
            {
                var result = await controller.Patch(Guid.NewGuid(), new ProductEditModel()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test product",
                    Price = 1
                });
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(OkResult));
            }

        }

        [TestMethod]
        //[ExpectedException(typeof(DbUpdateConcurrencyException))]
        public async Task Patch_Test_Conflict()
        {
            this.mapper.Setup(m => m.Map<ProductAddModel, Product>(It.IsAny<ProductAddModel>())).Returns(new Product()
            {
                Id = Guid.NewGuid()
            });

            Mock<IProductRepository> repositoryMock = new Mock<IProductRepository>();
            var dummyReadonlyList = new Mock<IReadOnlyList<IUpdateEntry>>();
            dummyReadonlyList.SetupGet(r => r.Count).Returns(1);

            this.unitOfWork.Setup(u => u.Products).Returns(repositoryMock.Object);
            this.unitOfWork.Setup(u => u.Update<Product>(It.IsAny<Product>()));
            this.unitOfWork.Setup(u => u.SaveAsync()).Throws(new DbUpdateConcurrencyException(It.IsAny<String>(), dummyReadonlyList.Object));

            using (var controller = new ProductsController(
                    this.unitOfWork.Object,
                    this.mapper.Object,
                    this.configuration.Object,
                    this.logger.Object,
                    this.distributedCache.Object))
            {
                var result = await controller.Patch(Guid.NewGuid(), new ProductEditModel()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test product",
                    Price = 1
                });

                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(ConflictResult));
            }

        }

        [TestMethod]
        public async Task Delete_OK_Test()
        {
            //Idempotent method, always OK

        }
    }
}
