﻿using Suitsupply.Catalog.FileStorage;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.FileProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.UnitTests
{
    [TestClass]
    public class StorageTests
    {
        private IFileStorageProvider fileStorageProvider = new MemoryStorageProvider();

        [TestMethod]
        public async Task AddPhoto()
        {
            //Add image
            var guid = Guid.NewGuid();
            var data = new byte[] { 1, 2, 3, 4, 5 };
            await this.fileStorageProvider.SaveAsync(guid, data);

            //Retrieve image
            var storedData = await this.fileStorageProvider.GetAsync(guid);

            Assert.IsTrue(storedData != null);
            Assert.IsTrue(storedData.Length == data.Length);
        }

        [TestMethod]
        public async Task RemovePhoto()
        {
            //Add image
            var guid = Guid.NewGuid();
            var data = new byte[] { 1, 2, 3, 4, 5 };
            await this.fileStorageProvider.SaveAsync(guid, data);

            //Retireve image
            var storedData = await this.fileStorageProvider.GetAsync(guid);

            Assert.IsTrue(storedData != null);
            Assert.IsTrue(storedData.Length == data.Length);

            //Remove image
            await this.fileStorageProvider.DeleteAsync(guid);

            //Try to retrieve
            storedData = await this.fileStorageProvider.GetAsync(guid);

            Assert.IsTrue(storedData == null);

        }


        [TestMethod]
        public async Task UpdatePhoto()
        {
            //Add image
            var guid = Guid.NewGuid();
            var data = new byte[] { 1, 2, 3, 4, 5 };
            await this.fileStorageProvider.SaveAsync(guid, data);

            //Retrieve image
            var storedData = await this.fileStorageProvider.GetAsync(guid);

            Assert.IsTrue(storedData != null);
            Assert.IsTrue(storedData.Length == data.Length);

            //Update image
            var newData = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            await this.fileStorageProvider.SaveAsync(guid, newData);

            //Retrieve image
            storedData = await this.fileStorageProvider.GetAsync(guid);

            Assert.IsTrue(storedData != null);
            Assert.IsTrue(storedData.Length == newData.Length);

        }

    }
}
