﻿using Suitsupply.Catalog.Data;
using Suitsupply.Catalog.Data.Entities;
using Suitsupply.Catalog.Data.UnitsOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.UnitTests
{
    [TestClass]
    public class ProductTests
    {
        private readonly CatalogDbContext dbContext = new CatalogDbContext(new DbContextOptionsBuilder<CatalogDbContext>()
                 .UseInMemoryDatabase(databaseName: "TestDatabase")
                 .Options);

        [TestCategory("Database")]
        [TestMethod]
        public async Task AddProduct()
        {
           using (var unitOfWork = new CatalogUnitOfWork(dbContext))
            {
                //Add product
                var guid = await unitOfWork.Products.InsertAsync(new Data.Entities.Product()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test",
                    Description = "Test product description",
                    LastUpdated = DateTime.Now.ToUniversalTime()
                });

                await unitOfWork.SaveAsync();

                //Retrieve product
                var product = await unitOfWork.Products.GetByIdAsync(guid);

                 Assert.IsTrue(product!=null);
            }

        }

        [TestCategory("Database")]
        [TestMethod]
        public async Task RemoveProduct()
        {
            using (var unitOfWork = new CatalogUnitOfWork(dbContext))
            {
                //Add product
                var guid = await unitOfWork.Products.InsertAsync(new Data.Entities.Product()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test",
                    Description = "Test product description",
                    LastUpdated = DateTime.Now.ToUniversalTime()
                });
                await unitOfWork.SaveAsync();

                //Retrieve product
                var product = await unitOfWork.Products.GetByIdAsync(guid);

                Assert.IsTrue(product != null);

                //Remove product
                unitOfWork.Products.Remove(guid);
                await unitOfWork.SaveAsync();

                //Retrieve product
                product = await unitOfWork.Products.GetByIdAsync(guid);

                Assert.IsTrue(product == null);
            }
        }


        [TestCategory("Database")]
        [TestMethod]
        public async Task EditProduct()
        {
            using (var unitOfWork = new CatalogUnitOfWork(dbContext))
            {
                //Add product 
                var guid = await unitOfWork.Products.InsertAsync(new Data.Entities.Product()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test",
                    Description = "Test product description",
                    LastUpdated = DateTime.Now.ToUniversalTime()
                });

                await unitOfWork.SaveAsync();

                //Retrieve product
                var product = await unitOfWork.Products.GetByIdAsync(guid);

                Assert.IsTrue(product != null);
                String newDescription = "This is updated description value";

                product.Description = newDescription;

                //Update product
                unitOfWork.Update<Product>(product);
                await unitOfWork.SaveAsync();

                //Retrieve product
                product = await unitOfWork.Products.GetByIdAsync(guid);

                Assert.IsTrue(product.Description.Equals(newDescription));
            }

        }

    }
}
