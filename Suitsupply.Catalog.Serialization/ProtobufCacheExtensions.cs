﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.Serialization
{
    public static class ProtobufCacheExtensions
    {
        static readonly ProtobufSerializer serializer =  new ProtobufSerializer();

        public static async Task<T> GetValueAsync<T>(this IDistributedCache cache, string key) where T : class
        {
            return serializer.Deserialize<T>(await cache.GetAsync(key));
        }

        public static async Task SetValueAsync<T>(this IDistributedCache cache, string key, T value, TimeSpan interval) where T : class
        {
            await cache.SetAsync(key,
                serializer.Serialize<T>(value),
                new DistributedCacheEntryOptions() { AbsoluteExpiration = DateTime.Now.AddSeconds(interval.TotalSeconds) });
        }

    }
}
