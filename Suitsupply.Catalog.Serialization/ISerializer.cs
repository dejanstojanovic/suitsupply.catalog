﻿namespace Suitsupply.Catalog.Serialization
{
    public interface ISerializer
    {
        byte[] Serialize<T>(T objectToSerialize) where T : class;
        T Deserialize<T>(byte[] arrayToDeserialize) where T : class;

    }
}
