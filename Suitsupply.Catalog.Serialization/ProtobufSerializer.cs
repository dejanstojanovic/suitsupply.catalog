﻿using ProtoBuf;
using System.IO;
using System.Linq;

namespace Suitsupply.Catalog.Serialization
{
    public class ProtobufSerializer:ISerializer
    {
        public T Deserialize<T>(byte[] arrayToDeserialize) where T:class
        {
            if (arrayToDeserialize != null && arrayToDeserialize.Any())
            {
                using (var memStream = new MemoryStream(arrayToDeserialize))
                {
                    return Serializer.Deserialize<T>(memStream);
                }
            }
            return null;
        }

        public byte[] Serialize<T>(T objectToSerialize) where T : class
        {
            if (objectToSerialize != null)
            {
                using (var memStream = new MemoryStream())
                {
                    Serializer.Serialize<T>(memStream, objectToSerialize);
                    return memStream.ToArray();
                }
            }
            return null;
        }
    }
}
