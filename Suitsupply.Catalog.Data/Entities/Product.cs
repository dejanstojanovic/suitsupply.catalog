﻿using Suitsupply.Catalog.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace Suitsupply.Catalog.Data.Entities
{
    public class Product:BaseEntity
    {
        [Required]
        public String Name { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }
        public DateTime LastUpdated { get; set; }
        [MaxLength(500)]
        public String Description { get; set; }
    }
}
