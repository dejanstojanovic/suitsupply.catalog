﻿using System.Threading.Tasks;
using Suitsupply.Catalog.Data.Entities;
using Suitsupply.Catalog.Data.Repositories;

namespace Suitsupply.Catalog.Data.UnitsOfWork
{
    /// <summary>
    /// IUnit of work implementation for product dbContext
    /// </summary>
    public class CatalogUnitOfWork : IUnitOfWork
    {
        CatalogDbContext dbContext;
        bool disposing;

        /// <summary>
        /// Products repository instance
        /// </summary>
        public IProductRepository Products { get; private set; }

        /// <summary>
        /// Creates transaction on the dbContext
        /// </summary>
        public IDatabaseTransaction BeginTransaction => new EntityDatabaseTransaction(this.dbContext);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext">Database context instance to apply the unit of work to</param>
        public CatalogUnitOfWork(CatalogDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.disposing = false;
            this.Products = new ProductsRepository(dbContext);
        }

        /// <summary>
        /// Updates existing entity
        /// </summary>
        /// <typeparam name="T">Type of entity to update</typeparam>
        /// <param name="entity">Entity instance of type T</param>
        public void Update<T>(T entity) where T : BaseEntity
        {
            this.dbContext.Set<T>().Update(entity);
        }

        /// <summary>
        /// Save changes made to dbContext
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            return this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Asynchronously save changes done in the dbContext
        /// </summary>
        /// <returns></returns>
        public async Task<int> SaveAsync()
        {
            return await this.dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Disposes Unit of work and inner components (dbContext)
        /// </summary>
        public void Dispose()
        {
            if (!this.disposing)
            {
                this.dbContext.Dispose();
                this.disposing = true;
            }
        }


    }
}
