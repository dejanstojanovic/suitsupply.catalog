﻿using Suitsupply.Catalog.Data.Entities;
using Suitsupply.Catalog.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Suitsupply.Catalog.Data.UnitsOfWork
{
    /// <summary>
    /// UNit of work for data actions
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Products repository instance
        /// </summary>
        IProductRepository Products { get; }

        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        void Update<T>(T entity) where T : BaseEntity;

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        int Save();

        /// <summary>
        /// Save change asynchronously
        /// </summary>
        /// <returns></returns>
        Task<int> SaveAsync();

        /// <summary>
        /// Creates trnasaction of the dbContext of the unitOfWork instance
        /// </summary>
        IDatabaseTransaction BeginTransaction { get; }
    }
}
