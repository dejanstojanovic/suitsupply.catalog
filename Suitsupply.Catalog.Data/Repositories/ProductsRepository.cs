﻿using Suitsupply.Catalog.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Suitsupply.Catalog.Data.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ProductsRepository : BaseRepository<Product>, IProductRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public ProductsRepository(CatalogDbContext dbContext) : base(dbContext)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override IQueryable<Product> Get()
        {
            return base.Get().OrderByDescending(p => p.LastUpdated);
        }

    }
}
