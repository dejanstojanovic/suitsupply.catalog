﻿using Suitsupply.Catalog.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Suitsupply.Catalog.Data.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
