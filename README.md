# Suitsupply.Catalog

Suitsupply test solution

## Highlights


#### Separated responsibility
Database and file data is handled by two separated services to avoid too monolithic service model

#### EF core with Repository and Unit of work 
Database is accessed through UnitOfWork instance injected in the controller constructor. 

#### Data concurrency 
Handled on the UnitOfWork level using custom GUID stored as version column

#### Performance concerns
Response cached and protobuf serialization used to gain on performance. Some serialization comparison results http://bit.ly/2JsCjQH I did few months back

#### REST API endpoint versioning
Multiple versions of the resource service using routes

#### Documentation
Resource service documented with Swagger

#### Sample client
MVC client (not state of art)

#### Unit tests
Basic unit tests for resource and images storage service and protobuf serializer

#### Integrated CI
Gitlab has integrated CI with the repositories, so you can see the status of the master branch buil

## Nice to have but the time is up


#### Authentication
GET actions on the API can be public, but POST and PATCH should be protected. I started working on identity in a separate branch [feature/identity](https://gitlab.com/dejanstojanovic/suitsupply.catalog/tree/feature/identity)

#### Expose OData for querying the service endpoint
Currently there is a simple GET for retrieving all Products. Ideally there should be OData of GraphQL on to of the service facade to expose result querying to the clients.

#### Better response handling in the client application
Currently client application lacks of validation and API response code handling, but my focus was on the services and data model and backend request handling.